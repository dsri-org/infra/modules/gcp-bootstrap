# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "gitlab_project" "this" {
  for_each = local.gcp_service_accounts_map
  name     = var.gitlab_project_path
  #namespace_id = data.gitlab_group.this[dirname(each.key)].group_id

  allow_merge_on_skipped_pipeline                  = false
  analytics_access_level                           = "disabled"
  auto_cancel_pending_pipelines                    = "enabled"
  autoclose_referenced_issues                      = true
  build_git_strategy                               = "fetch"
  builds_access_level                              = "enabled"
  ci_default_git_depth                             = 1
  ci_forward_deployment_enabled                    = true
  ci_restrict_pipeline_cancellation_role           = "developer"
  ci_separated_caches                              = false
  container_registry_access_level                  = "disabled"
  environments_access_level                        = "enabled"
  feature_flags_access_level                       = "disabled"
  infrastructure_access_level                      = "enabled"
  initialize_with_readme                           = false
  issues_access_level                              = "enabled"
  issues_enabled                                   = true
  lfs_enabled                                      = false
  merge_method                                     = "merge"
  merge_pipelines_enabled                          = false
  merge_requests_access_level                      = "enabled"
  merge_trains_enabled                             = false
  monitor_access_level                             = "disabled"
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  packages_enabled                                 = false
  pages_access_level                               = "disabled"
  printing_merge_request_link_enabled              = true
  public_jobs                                      = false
  releases_access_level                            = "disabled"
  remove_source_branch_after_merge                 = true
  repository_access_level                          = "enabled"
  request_access_enabled                           = false
  requirements_access_level                        = "disabled"
  resolve_outdated_diff_discussions                = true
  restrict_user_defined_variables                  = true
  security_and_compliance_access_level             = "disabled"
  snippets_access_level                            = "disabled"
  snippets_enabled                                 = false
  squash_option                                    = "always"
  visibility_level                                 = "public"
  wiki_access_level                                = "disabled"
  wiki_enabled                                     = false

  push_rules {
    commit_committer_check = true
    deny_delete_tag        = true
    max_file_size          = 1
    member_check           = true
    prevent_secrets        = true
  }
}

# gitlab "used by" feature
resource "gitlab_project_job_token_scope" "used_by" {
  #for_each = local.gitlab_project_consumer_ids

  project           = var.gitlab_project_id
  target_project_id = var.gitlab_project_id
}

resource "google_project_service" "this" {
  for_each = local.gcp_service_accounts_map

  project = var.gcp_project_id
  service = each.key
}

module "service_accounts" {
  for_each = local.gcp_service_accounts_map

  source = "../gcp-workload-service-account"

  #project_id        = var.gcp_project_id
  gitlab_project_id = var.gitlab_project_id
  # name              = var.gitlab_project_name
  #repository        = var.gitlab_project_path
  environment               = var.environment
  gcp_service_account_name  = var.gcp_service_account_name
  gcp_project_id            = var.gcp_project_id
  gcp_service_account_roles = var.gcp_service_account_roles
  deployment                = var.deployment

  roles = each.value.roles
}
