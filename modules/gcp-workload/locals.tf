# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  gcp_service_accounts_map = {
    for service_account in var.gcp_service_accounts : service_account.repository => service_account
  }
}
