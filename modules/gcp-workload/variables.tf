# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# var gcp_folder
variable "gcp_folder" {
  type = string
}

# var gitlab_project_id
variable "gitlab_project_id" {
  type = string
}

# var gitlab_project_path
variable "gitlab_project_path" {
  type = string
}

# var gitlab_project_name
variable "gitlab_project_name" {
  type = string
}

# var gitlab_project_path_with_namespace = ul-dsri/infra/dyff/gcp
variable "gitlab_project_path_with_namespace" {
  type    = string
  default = "ul-dsri/infra/dyff/gcp"
}

# var gcp_project_id
variable "gcp_project_id" {
  type = string
}

# var environment = staging, production
variable "environment" {
  type = string
}

# var gcp_service_accounts
variable "gcp_service_accounts" {
  #type = string
}

# var gitlab_project_consumers gitlab "used by"
variable "gitlab_project_consumers" {
  type = string
}

variable "gcp_service_account_roles" {
  type = string
}

variable "gcp_service_account_name" {
  type = string
}

variable "deployment" {
  type = string
}
