# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "google_service_account" "this" {
  project      = var.gcp_project_id
  account_id   = "tf-${var.gcp_service_account_name}"
  display_name = "tf-${var.gcp_service_account_name}"
}

resource "google_service_account_key" "this" {
  service_account_id = google_service_account.this.name
}

resource "google_project_iam_member" "service_accounts" {
  for_each = local.roles_map

  project = var.gcp_project_id
  role    = each.value.name
  member  = google_service_account.this.member
}

resource "gitlab_project_variable" "service_account_file" {
  project           = var.gitlab_project_id
  key               = "TF_VAR_google_cloud_service_account_file"
  environment_scope = "${var.environment}/*"
  value             = base64decode(google_service_account_key.this.private_key)
  protected         = false
  masked            = false
  variable_type     = "file"
}
