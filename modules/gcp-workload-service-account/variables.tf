# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

#var gcp_service_account_name
variable "gcp_service_account_name" {
  type = string
}

#var gitlab_project_id
variable "gitlab_project_id" {
  type = string
}

#var gcp_project_id
variable "gcp_project_id" {
  type = string
}

#var gcp_service_account_roles
variable "gcp_service_account_roles" {
  type = string
}

#var deployment
variable "deployment" {
  type = string
}

variable "environment" {
  type = string
}

variable "roles" {
  type = list(object({
    name = string
  }))
}
